import React from 'react';
import PropTypes from 'prop-types';
import {requireNativeComponent, View} from 'react-native';

var ScrollChoiceView = requireNativeComponent('ScrollChoice', ScrollChoice);

export default class ScrollChoice extends React.Component {
    constructor(props) {
        super(props);
        this.onItemSelected = this.onItemSelected.bind(this);
        this.state = {
            selectedItemPosition: 0
        }
    }

    onItemSelected(event) {
        if (!this.props.onItemSelected) {
            return;
        }
        this.props.onItemSelected(event.nativeEvent);
    }

    componentDidMount() {
        this.setState({selectedItemPosition: this.props.selectedItemPosition})
    }

    render() {
        return (
            <ScrollChoiceView
                {...this.props}
                data={this.props.data}
            />
        );
    }
}

ScrollChoice.propTypes = {
    ...View.propTypes,
    data: PropTypes.array,
};
