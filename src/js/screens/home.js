import React from 'react';
import {View, Platform} from 'react-native';
import WheelPicker from '../components/WheelPicker';

export default class extends React.Component {
    static navigationOptions = {
        title: 'Home'
    };

    constructor(props) {
        super(props);

        this.onPickerSelect = this.onPickerSelect.bind(this);

        this.state = {
            selectedItem: '2',
        };
    }

    onPickerSelect(selectedItem) {
        this.setState({selectedItem})
    }

    render() {
        const arr = [1, 2, 3, 4, 5, 6];

        return (
            <View style={{
                justifyContent: 'center',
                alignItems: 'center',
            }}>

                {
                    Platform.OS === 'android' && <WheelPicker
                        onItemSelected={(event) => {
                            console.log(event)
                        }}
                        isCurved
                        renderIndicator
                        selectedItemPosition={3}
                        itemTextSize={50}
                        data={arr}
                        selectedItemTextColor={'green'}
                        indicatorColor={'green'}
                        style={{width: 100, height: 100}}/>
                }
            </View>
        );
    }
}
