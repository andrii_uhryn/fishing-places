#import "JTSImagePreview.h"
#import "React/RCTLog.h"
#import <JTSImageViewController.h>
#import "AppDelegate.h"

@implementation JTSImagePreview

RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(showImage:(NSString *)url)
{
  RCTLogInfo(@"showImage with url %@", url);
  
  JTSImageInfo *imageInfo = [[JTSImageInfo alloc] init];
  imageInfo.imageURL = [NSURL URLWithString:url];
  
  JTSImageViewController *imageViewer = [[JTSImageViewController alloc]
                                         initWithImageInfo:imageInfo
                                         mode:JTSImageViewControllerMode_Image
                                         backgroundStyle:JTSImageViewControllerBackgroundOption_None];
  
  AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
  
  UIViewController* showingController;
  if([delegate.window.rootViewController isKindOfClass:[UINavigationController class]]){
    
    showingController = ((UINavigationController*)delegate.window.rootViewController).visibleViewController;
  } else if ([delegate.window.rootViewController isKindOfClass:[UITabBarController class]]) {
    
    showingController = ((UITabBarController*)delegate.window.rootViewController).selectedViewController;
  } else {
    
    showingController = (UIViewController*)delegate.window.rootViewController;
  }
  
  dispatch_async(dispatch_get_main_queue(), ^{
    [imageViewer showFromViewController:showingController transition:JTSImageViewControllerTransition_FromOffscreen];
  });
}

@end
