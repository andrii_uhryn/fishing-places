import React from 'react';
import {AppRegistry} from 'react-native';
import {StackNavigator} from 'react-navigation';

import HomeScreen from './src/js/screens/home';

const FishingApp = StackNavigator({
    Home: {
        screen: HomeScreen,
    }
});

AppRegistry.registerComponent('fishingPlaces', () => FishingApp);
